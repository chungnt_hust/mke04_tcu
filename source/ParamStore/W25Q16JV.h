/*
 * W25Q16JV.h
 *
 *  Created on: Dec 13, 2018
 *      Author: chungnguyen
 */

#ifndef SPI_W25Q16JV_H_
#define SPI_W25Q16JV_H_
#include "MKE04Z1284.h"

#define WRITE_ENABLE     0x06
#define WRITE_DISABLE    0x04
#define READ_DATA        0x03
#define WRITE_PAGE       0x02
#define SECTOR4K         0x20
#define BLOCK32K         0x52
#define BLOCK64K         0xd8
#define CHIP_ERASE       0xd8
#define READ_SR1         0x05
#define WRITE_SR1        0x01
#define READ_SR2         0x35
#define WRITE_SR2        0x31
#define READ_SR3         0x15
#define WRITE_SR3        0x11

/* flash 2Mbyte = 2048Kbyte */
/* sector erase 4k: 512 base address */
#define ERASE_SECTOR_ADD(x) (0x000000+0x001000*x)
/* sector erase 16k: 128 base address */
#define ERASE_BLOCK16K_ADD(x) (0x000000+0x004000*x)
/* sector erase 64k: 32 base address */
#define ERASE_BLOCK64K_ADD(x) (0x000000+0x010000*x)
/* PID parameter address*/
#define PID_PARAMS_ADD ERASE_SECTOR_ADD(100)


/* @biref: write to flash
 * @param: add: address start to transmit data, sector erase
 * @param: txData: pointer point to array data
 * @param: len: size of data
 * @reval: NONE
 */
void W25Q16JV_Write(uint32_t add, void *txData, uint32_t len);

/* @biref: delay 100ms for write, wait erase success. Check every 10ms
 * @param: NONE
 * @reval: NONE
 */
void W25Q16JV_WriteProcess(void);

/* @biref: read from flash
 * @param: add: address start to receive data
 * @param: txData: pointer point to array data
 * @param: len: size of data
 * @reval: NONE
 */
void W25Q16JV_Read(uint32_t add, void *rxData, uint32_t len);

/* @biref: erase block 4k
 * @param: add: header address of the block
 * @reval: NONE
 */
void W25Q16JV_erase4k(uint32_t add);
#endif /* SPI_W25Q16JV_H_ */
