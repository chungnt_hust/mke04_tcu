/*
 * W25Q16JV.c
 *
 *  Created on: Dec 13, 2018
 *      Author: chungnguyen
 */
#include <stdio.h>
#include "SpiUser.h"
#include "W25Q16JV.h"
#include "fsl_gpio.h"
#include "timeCheck.h"
/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define TIME_WAIT_EARSE_DONE 100
/*******************************************************************************
 * Variables
 ******************************************************************************/
enum
{
	FLASH_PIN = 0,
} flashPin_t;
static GPIO_Type *gpioPort[] = {GPIOB};
static uint32_t valuePin[] = {1<<19};

struct
{
	bool writeEnable;
	uint32_t timeWaitEraseDone;
	uint8_t temArr[4];
	uint8_t *temPtr;
	uint32_t temLength;
} flashWrite_Type;

typedef enum
{
	ERASE_4K = 0,
	ERASE_16K,
	ERASE_64K
} erase_type_t;

extern volatile uint32_t g_SysTime;
/*******************************************************************************
 * Code
 ******************************************************************************/
void flashSelect(void)
{
	gpioPort[FLASH_PIN]->PCOR = valuePin[FLASH_PIN];
}

void flashDisSelect(void)
{
	gpioPort[FLASH_PIN]->PSOR = valuePin[FLASH_PIN];
}

void flashWriteEnable(void)
{
	flashSelect();
	uint8_t tx = WRITE_ENABLE;
	SPI_User_Write(&tx, 1);
	flashDisSelect();
}

void flashWriteDisable(void)
{
	flashSelect();
	uint8_t tx = WRITE_DISABLE;
	SPI_User_Write(&tx, 1);
	flashDisSelect();
}

void W25Q16JV_Write(uint32_t add, void *txData, uint32_t len)
{
	flashWrite_Type.temArr[0] = WRITE_PAGE;
	flashWrite_Type.temArr[1] = (uint8_t)(add>>16);
	flashWrite_Type.temArr[2] = (uint8_t)(add>>8);
	flashWrite_Type.temArr[3] = (uint8_t)add;
	flashWrite_Type.temPtr = (uint8_t*)txData;
	flashWrite_Type.temLength = len;

	W25Q16JV_erase4k(add);
	flashWrite_Type.writeEnable = true;
	flashWrite_Type.timeWaitEraseDone = g_SysTime + TIME_WAIT_EARSE_DONE;
}

void W25Q16JV_WriteProcess(void)
{
	if(flashWrite_Type.writeEnable)
	{
		if(checkPassTime(g_SysTime, flashWrite_Type.timeWaitEraseDone))
		{
			flashWrite_Type.writeEnable = false;
			flashWriteEnable();
			flashSelect();
			SPI_User_Write(flashWrite_Type.temArr, 4);
			SPI_User_Write(flashWrite_Type.temPtr, flashWrite_Type.temLength);
			flashDisSelect();
			flashWriteDisable();
		}
	}
}

void W25Q16JV_Read(uint32_t add, void *rxData, uint32_t len)
{
	uint8_t *p;
	uint8_t arr[4];
	arr[0] = READ_DATA;
	arr[1] = (uint8_t)(add>>16);
	arr[2] = (uint8_t)(add>>8);
	arr[3] = (uint8_t)add;

	flashSelect();
	SPI_User_Write(arr, 4);

	p = (uint8_t*)rxData;
	SPI_User_Read(p, len);
	flashDisSelect();
}

void W25Q16JV_erase4k(uint32_t add)
{
	uint8_t arr[4];
	arr[0] = SECTOR4K;
	arr[1] = add>>16;
	arr[2] = add>>8;
	arr[3] = add;

	flashWriteEnable();
	flashSelect();
	SPI_User_Write(arr, 4);
	flashDisSelect();
	flashWriteDisable();
}

