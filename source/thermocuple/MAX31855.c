/*
 * MAX31855.c
 *
 *  Created on: Dec 21, 2018
 *      Author: chungnguyen
 */
#include "MAX31855.h"
#include "SpiUser.h"
#include "display.h"
#include <math.h>
/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define SENSITIVITY 0.041276 // mV/*C
/*******************************************************************************
 * Variables
 ******************************************************************************/
enum
{
	THERMO_PIN = 0,
} thermoPin_t;
static GPIO_Type *gpioPort[] = {GPIOA};
static uint32_t valuePin[] = {1<<0};

enum
{
	NO_ERROR = 0,
	SHORT_TO_VCC = 4,
	SHORT_TO_GND = 2,
	OPEN_CURRENT = 1
} error_t;
/*******************************************************************************
 * Code
 ******************************************************************************/
static double thermocoupleVoltage(double internalTem, double thermocoupleTem);
static double internalVoltage(double internalTem);
static double totalVoltage(double internalTem, double thermocoupleTem);
static double readTemperature(double internalTem, double thermocoupleTem);

void thermoSelect(void)
{
	gpioPort[THERMO_PIN]->PCOR = valuePin[THERMO_PIN];
}

void thermoDisSelect(void)
{
	gpioPort[THERMO_PIN]->PSOR = valuePin[THERMO_PIN];
}

bool MAX31855_readTemperature(int16_t *internalTem, int16_t *thermocoupleTem)
{
	bool err = false;
	double temperature, intem, thertem;
	dataReceive_t xData;
	thermoSelect();
	SPI_User_Read(&xData.byte[3], 1);
	SPI_User_Read(&xData.byte[2], 1);
	SPI_User_Read(&xData.byte[1], 1);
	SPI_User_Read(&xData.byte[0], 1);
	thermoDisSelect();

//	if(xData.data & OPEN_CURRENT)
//	{
//			openCurrent();
//			err = true;
//	}
//	else if(xData.data & SHORT_TO_GND)
//	{
//			shortToGND();
//			err = true;
//	}
//	else if(xData.data & SHORT_TO_VCC)
//	{
//			shortToVCC();
//			err = true;
//	}
//	else
//	{
		/* read 12bit of internal temperature */
		xData.data >>= 4;
		/* bit 12 is sign bit */
		temperature = xData.data & 0x7FF;
		/* Check for negative temperature */
		if((xData.data & 0x800) != 0)
		{
			temperature = temperature - 2048;
		}

		/* convert to degree celsius */
		temperature = temperature*0.0625;
		intem = temperature;
		*internalTem = (int16_t)intem;

		temperature = 0;
		/* read 18 bit of thermocouple temperature */
		xData.data >>= 14;
		/* bit 14 is sign bit */
		temperature = xData.data & 0x1FFF;
		/* Check for negative temperature */
		if((xData.data & 0x2000) != 0)
		{
			temperature = temperature - 8192;
		}

		/* convert to degree celsius */
		temperature = temperature*0.25;
		thertem = temperature;

//		*thermocoupleTem = (int16_t)readTemperature(intem, thertem);
		*thermocoupleTem = (int16_t)thertem;
		err = false;
//	}
	return err;
}

static double thermocoupleVoltage(double internalTem, double thermocoupleTem)
{
	return ((thermocoupleTem-internalTem)*SENSITIVITY);
}

static double internalVoltage(double internalTem)
{
	double mV = 0;
	if(internalTem < 0)
	{
		double c[11] = { 0.000000000000E+00,  0.394501280250E-01,
					     0.236223735980E-04, -0.328589067840E-06,
						-0.499048287770E-08, -0.675090591730E-10,
						-0.574103274280E-12, -0.310888728940E-14,
						-0.104516093650E-16, -0.198892668780E-19, -0.163226974860E-22};
		for(uint8_t i = 0; i < 11; i++)
		{
			mV += c[i]*pow(internalTem, i);
		}
	}
	else
	{
		double c[10] = {-0.176004136860E-01, 0.389212049750E-01,
						 0.185587700320E-04, -0.994575928740E-07,
						 0.318409457190E-09, -0.560728448890E-12,
						 0.560750590590E-15, -0.320207200030E-18,
						 0.971511471520E-22, -0.121047212750E-25};
        double a0 =  0.118597600000E+00;
        double a1 = -0.118343200000E-03;
        double a2 =  0.126968600000E+03;
        for(uint8_t i = 0; i < 10; i++)
        {
        	mV += c[i]*pow(internalTem, i);
        }
        mV += a0*exp(a1*pow((internalTem - a2), 2));
	}
	return mV;
}

static double totalVoltage(double internalTem, double thermocoupleTem)
{
	return (thermocoupleVoltage(internalTem, thermocoupleTem) + internalVoltage(internalTem));
}

static double readTemperature(double internalTem, double thermocoupleTem)
{
	double tem;
	double voltage = totalVoltage(internalTem, thermocoupleTem);
	if(voltage < 0)
	{
		double d[9] = { 0.0000000E+00, 2.5173462E+01,
					  -1.1662878E+00, -1.0833638E+00,
					  -8.9773540E-01, -3.7342377E-01,
					  -8.6632643E-02, -1.0450598E-02, -5.1920577E-04};
		for(uint8_t i = 0; i < 9; i++)
		{
			tem += d[i]*pow(voltage, i);
		}
	}
	else if(voltage < 20.644)
	{
		double d[10] = {0.000000E+00, 2.508355E+01,
				 	    7.860106E-02, -2.503131E-01,
					    8.315270E-02, -1.228034E-02,
					    9.804036E-04, -4.413030E-05,
					    1.057734E-06, -1.052755E-08};
		for(uint8_t i = 0; i < 10; i++)
		{
			tem += d[i]*pow(voltage, i);
		}
	}
	else if(voltage < 54.886)
	{
		double d[7] = {-1.318058E+02, 4.830222E+01,
				      -1.646031E+00, 5.464731E-02,
					  -9.650715E-04, 8.802193E-06, -3.110810E-08};
		for(uint8_t i = 0; i < 7; i++)
		{
			tem += d[i]*pow(voltage, i);
		}
	}
	return tem;
}
