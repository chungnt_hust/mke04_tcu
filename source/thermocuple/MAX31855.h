/*
 * MAX31855.h
 *
 *  Created on: Dec 21, 2018
 *      Author: chungnguyen
 */
/* reference: https://github.com/rocketscream/MAX31855/blob/master/MAX31855.cpp */
#ifndef SPI_MAX31855_H_
#define SPI_MAX31855_H_
#include "MKE04Z1284.h"
#include <stdbool.h>
typedef union
{
	uint32_t data;
	uint8_t  byte[4];
} dataReceive_t ;

bool MAX31855_readTemperature(int16_t *internalTem, int16_t *thermocoupleTem);
void READ_PROCESS(void);
#endif /* SPI_MAX31855_H_ */
