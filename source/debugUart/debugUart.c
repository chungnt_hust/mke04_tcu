/*
 * debugUart.c
 *
 *  Created on: Feb 15, 2019
 *      Author: chungnguyen
 */
#include <debugUart/debugUart.h>

void debugUart_Init(void)
{
	DbgConsole_Init((uint32_t)UART1, 115200, DEBUG_CONSOLE_DEVICE_TYPE_UART, CLOCK_GetFreq(kCLOCK_BusClk));
	printf("init done\r\n");
}
