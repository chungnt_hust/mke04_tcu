/*
 * debugUart.h
 *
 *  Created on: Feb 15, 2019
 *      Author: chungnguyen
 */

#ifndef DEBUGUART_DEBUGUART_H_
#define DEBUGUART_DEBUGUART_H_
#include <fsl_debug_console.h>
#include "MKE04Z1284.h"
#include "fsl_uart.h"
void debugUart_Init(void);


#endif /* DEBUGUART_DEBUGUART_H_ */
