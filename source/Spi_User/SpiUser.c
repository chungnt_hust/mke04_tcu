/*
 * user_spi.c
 *
 *  Created on: Dec 13, 2018
 *      Author: chungnguyen
 */
#include <stdio.h>
#include "SpiUser.h"
#include "fsl_spi.h"

#define SPI_0 SPI0
#define SOUCE_CLOCK   kCLOCK_BusClk
#define CLK_REQ       CLOCK_GetFreq(kCLOCK_BusClk)
#define IRQ_number    SPI0_IRQn
#define ISR_SIP0      SPI0_IRQHandler

void SPI_User_Init(void)
{
    /* Init SPI master */
    /*
     * masterConfig->enableStopInWaitMode = false;
     * masterConfig->polarity = kSPI_ClockPolarityActiveHigh;
     * masterConfig->phase = kSPI_ClockPhaseFirstEdge;
     * masterConfig->direction = kSPI_MsbFirst;
     * masterConfig->dataMode = kSPI_8BitMode;
     * masterConfig->txWatermark = kSPI_TxFifoOneHalfEmpty;
     * masterConfig->rxWatermark = kSPI_RxFifoOneHalfFull;
     * masterConfig->pinMode = kSPI_PinModeNormal;
     * masterConfig->outputMode = kSPI_SlaveSelectAutomaticOutput;
     * masterConfig->baudRate_Bps = 500000U;
     */
	spi_master_config_t masterConfig;
	SPI_MasterGetDefaultConfig(&masterConfig);
	masterConfig.outputMode = kSPI_SlaveSelectAsGpio;
	masterConfig.baudRate_Bps = 500000U;
	SPI_MasterInit(SPI_0, &masterConfig, CLK_REQ);
	//SPI_MasterTransferCreateHandle(SPI_0, &handle, masterCallback, NULL);
	//printf("clk = %d\n", CLK_REQ);
	printf("Init SPI done!!\r\n");
}

void SPI_User_Write(uint8_t *txData, size_t dataSize)
{
	spi_transfer_t xfer;
	xfer.txData = txData;
	xfer.rxData = 0;
	xfer.dataSize = dataSize;
	SPI_MasterTransferBlocking(SPI_0, &xfer);
}

void SPI_User_Read(uint8_t *rxData, size_t dataSize)
{
	spi_transfer_t xfer;
	xfer.txData = 0;
	xfer.rxData = rxData;
	xfer.dataSize = dataSize;
	SPI_MasterTransferBlocking(SPI_0, &xfer);
}
