/*
 * user_spi.h
 *
 *  Created on: Dec 13, 2018
 *      Author: chungnguyen
 */

#ifndef SPI_USER_SPI_H_
#define SPI_USER_SPI_H_
#include "MKE04Z1284.h"

/* @biref: init module SPI0
 * @param: NONE
 * @reval: NONE
 */
void SPI_User_Init(void);

/* @biref: transmit dataSize byte to slave
 * @param: txData: pointer point to array data
 * @param: dataSize: number of data will be transmit
 * @reval: NONE
 */
void SPI_User_Write(uint8_t *txData, size_t dataSize);

/* @biref: receive data from slave
 * @param: rxData: pointer point to array data
 * @param: dataSize: number of data will be receive
 * @reval: NONE
 */
void SPI_User_Read(uint8_t *rxData, size_t dataSize);
#endif /* SPI_USER_SPI_H_ */
