/*
 * keyboard.c
 *
 *  Created on: Feb 13, 2019
 *      Author: chungnguyen
 */
#include <debugUart/debugUart.h>
#include "keyboard.h"
#include "display.h"
#include "Ui_control.h"
#include "W25Q16JV.h"
#include "timeCheck.h"
#include <stdbool.h>
#include "algorithm/pid/pid.h"
#include "algorithm/controller/controller.h"
/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define TIME_WAIT_MAX   30000
/*******************************************************************************
 * Variables
 ******************************************************************************/
static GPIO_Type *gpioPortRow[] = {GPIOB, GPIOA, GPIOB, GPIOB, GPIOA};
static uint32_t valuePinRow[]   = {11,    7,     4,     8,     26};
static GPIO_Type *gpioPortCol[] = {GPIOB, GPIOA, GPIOB, GPIOA, GPIOA, GPIOA, GPIOA, GPIOA};
static uint32_t valuePinCol[]   = {1<<10, 1<<6,  1<<9,  1<<28, 1<<27, 1<<3,  1<<2,  1<<1};
struct
{
	uint8_t state;
	bool isPress;
	uint8_t timeHold;
} button[MAX_ROW*MAX_COL];
enum
{
	IDLE = 0,
	PRESS,
	RELEASE
};
typedef struct
{
	void (*func)(uint8_t);
	uint8_t num;
} cb_t;
enum
{
	NONE = 0,
	A_MODE,
	B_MODE,
	C_MODE,
	D_MODE,
	E_MODE,
	F_MODE
};
uint8_t controlState;
uint8_t chuTrinhTam;
uint8_t phanDoanTam;
int16_t temTime[20], temTemp[20];
int16_t pidParam[3];
uint8_t chiSoPid;
uint8_t pos;
bool hasPress;
uint32_t timeWaitReset;
extern volatile uint32_t g_SysTime;
extern uint8_t Ui_State;
extern uint8_t chuTrinh;
extern uint8_t phanDoan;
extern params_t availableSegment[10];
extern params_t unavailableSegment[10];
extern uint8_t RunningMode;
extern uint8_t Ui_State;
extern pid_param_t PID_param ;
extern uint8_t lat;
/*******************************************************************************
 * Code
 ******************************************************************************/
uint8_t KB_readRow(pinRow_t row)
{
	return (((gpioPortRow[row]->PDIR) >> valuePinRow[row]) & 0x01);
}

void KB_setCol(pinCol_t col)
{
	gpioPortCol[col]->PSOR = valuePinCol[col];
}

void KB_resetCol(pinCol_t col)
{
	gpioPortCol[col]->PCOR = valuePinCol[col];
}

void KB_selectCol(pinCol_t col)
{
	for(uint8_t i = 0; i < MAX_COL; i++)
	{
		if(i == col) KB_resetCol(i);
		else KB_setCol(i);
	}
}

/* define callback function */
void BTNA_cb(uint8_t num);
void BTNB_cb(uint8_t num);
void BTNC_cb(uint8_t num);
void BTND_cb(uint8_t num);
void BTNE_cb(uint8_t num);
void BTNF_cb(uint8_t num);
void BTNnum_cb(uint8_t num);
void LEFT_cb(uint8_t num);
void RIGHT_cb(uint8_t num);
void ENTER_cb(uint8_t num);
void ESC_cb(uint8_t num);
void CLEAR_cb(uint8_t num);
void PAUSE_cb(uint8_t num);
void NEGATIVE_cb(uint8_t num);
void PGUP_cb(uint8_t num);
void PGDN_cb(uint8_t num);

const cb_t btnCallback[MAX_ROW*MAX_COL] =
{	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},
	{0, 0},

	{BTNA_cb, 0xAA},
	{BTNB_cb, 0xAA},
	{BTNnum_cb, 1},
	{BTNnum_cb, 2},
	{BTNnum_cb, 3},
	{BTNnum_cb, 9},
	{BTNnum_cb, 5},
	{0, 0},

	{BTNC_cb, 0xAA},
	{BTND_cb, 0},
	{BTNnum_cb, 4},
	{0, 0},
	{LEFT_cb, 0},
	{0, 0},
	{ESC_cb, 0},
	{CLEAR_cb, 0},

	{BTNE_cb, 0xAA},
	{BTNnum_cb, 7},
	{BTNnum_cb, 0},
	{0, 0},
	{PAUSE_cb, 0},
	{0, 0},
	{NEGATIVE_cb, 0},
	{BTNnum_cb, 6},

	{BTNF_cb, 0xAA},
	{BTNnum_cb, 8},
	{PGUP_cb, 0},
	{0, 0},
	{RIGHT_cb, 0},
	{0, 0},
	{ENTER_cb, 0},
	{PGDN_cb, 0}
};
void KB_callBack(uint8_t btnId)
{
	if(btnCallback[btnId].func != 0)
	{
		if(btnCallback[btnId].num == 0xAA) Ui_State = SETTING;
		btnCallback[btnId].func(btnCallback[btnId].num);
	}
}

void KB_scan(void)
{
	for(uint8_t col = 0; col < MAX_COL; col++)
	{
		KB_selectCol(col);
		for(uint8_t row = 0; row < MAX_ROW; row++)
		{
			uint8_t btnId = MAX_COL*row + col;
			switch(button[btnId].state)
			{
				case IDLE:
					if(KB_readRow(row) == 0) button[btnId].state = PRESS;
					break;
				case PRESS:
					if(++button[btnId].timeHold >= 50)
					{
						button[btnId].timeHold = 0;
						if(KB_readRow(row) == 0)
						{
							//callback;
							KB_callBack(btnId);
							button[btnId].state = RELEASE;
						}
						else button[btnId].state = IDLE;
					}
					break;
				case RELEASE:
					if(KB_readRow(row) == 1) button[btnId].state = IDLE;
					break;
			}
		}
	}
}

void BTNA_cb(uint8_t num)
{
	switch(controlState)
	{
		case NONE:
			KB_enableCheckTimeOut();
			display_show_program(PRO_A_MODE);
			controlState = A_MODE;
			break;
	}
}

void BTNB_cb(uint8_t num)
{
	switch(controlState)
	{
		case NONE:
			KB_enableCheckTimeOut();
			display_show_program(PRO_B_MODE);
			controlState = B_MODE;
			break;
	}
}

void BTNC_cb(uint8_t num)
{
	switch(controlState)
	{
		case NONE:
			KB_enableCheckTimeOut();
			display_show_program(PRO_C_MODE);
			pos = 7;
			for(uint8_t i = 0; i < 20; i++) temTime[i] = temTemp[i] = 0;
			phanDoanTam = 0;
			controlState = C_MODE;
			break;
	}
}

void BTND_cb(uint8_t num)
{
	switch(controlState)
	{
		case C_MODE:
			KB_enableCheckTimeOut();
			display_show_program(PRO_B_MODE);
			controlState = D_MODE;
			break;
		case D_MODE:
			KB_cancleCheckTimeOut();
			phanDoanTam = phanDoanTam + 1;
			for(uint8_t i = 0; i < 20; i++)
			{
				unavailableSegment[chuTrinhTam].temSet[i] = temTemp[i];
				unavailableSegment[chuTrinhTam].timeSet[i] = temTime[i];
			}
			unavailableSegment[chuTrinhTam].maxSeg = phanDoanTam;
			W25Q16JV_Write(ERASE_SECTOR_ADD(chuTrinhTam), (uint8_t*)&unavailableSegment[chuTrinhTam], sizeof(unavailableSegment[chuTrinhTam]));
			Ui_returnLastState();
			controlState = NONE;
			break;
	}
}

void BTNE_cb(uint8_t num)
{
	switch(controlState)
	{
		case NONE:
			KB_enableCheckTimeOut();
			display_show_program(PRO_E_MODE);
			phanDoanTam = 0;
			controlState = E_MODE;
			break;
	}
}

void BTNF_cb(uint8_t num)
{
	switch(controlState)
	{
		case NONE:
			KB_enableCheckTimeOut();
			chiSoPid = 0;
			display_setPID(chiSoPid, pidParam[chiSoPid]);
			controlState = F_MODE;
			break;
	}
}

void BTNnum_cb(uint8_t num)
{
	switch(controlState)
	{
		case A_MODE:
			KB_enableCheckTimeOut();
			chuTrinhTam = num;
			Led7_showCharAt(A_code, 6);
			Led7_showCharAt(num, 7);
			break;
		case B_MODE:
			KB_enableCheckTimeOut();
			chuTrinhTam = num;
			Led7_showCharAt(b_code, 6);
			Led7_showCharAt(num, 7);
			break;
		case C_MODE:
			KB_enableCheckTimeOut();
			if(pos < 4)
			{
				if(temTemp[phanDoanTam] >= 0) temTemp[phanDoanTam] = (temTemp[phanDoanTam]*10+num)%10000;
				else if(temTemp[phanDoanTam] > - 100) temTemp[phanDoanTam] = (temTemp[phanDoanTam]*10-num)%10000;
				display_leftParam(temTemp[phanDoanTam]);
			}
			else
			{
				temTime[phanDoanTam] = (temTime[phanDoanTam]*10+num)%10000;
				display_rightParam(temTime[phanDoanTam]);
			}
			break;
		case D_MODE:
			KB_enableCheckTimeOut();
			chuTrinhTam = num;
			Led7_showCharAt(b_code, 6);
			Led7_showCharAt(num, 7);
			break;
		case E_MODE:
			KB_enableCheckTimeOut();
			chuTrinhTam = num;
			display_leftParam(unavailableSegment[chuTrinhTam].temSet[phanDoanTam]);
			display_rightParam(unavailableSegment[chuTrinhTam].timeSet[phanDoanTam]);
			Led7_canCleBlink(6);
			Led7_canCleBlink(7);
			SingleLed_On(phanDoanTam%10);
			break;
		case F_MODE:
			KB_enableCheckTimeOut();
			pidParam[chiSoPid] = (pidParam[chiSoPid]*10+num)%10000;
			display_setPID(chiSoPid, pidParam[chiSoPid]);
			break;
	}
}

void LEFT_cb(uint8_t num)
{
	switch(controlState)
	{
		case C_MODE:
			KB_enableCheckTimeOut();
			pos = 3;
			Led7_canCleBlink(7);
			Led7_blink(3);
			break;
	}
}

void RIGHT_cb(uint8_t num)
{
	switch(controlState)
	{
		case C_MODE:
			KB_enableCheckTimeOut();
			pos = 7;
			Led7_canCleBlink(3);
			Led7_blink(7);
			break;
	}
}

void ENTER_cb(uint8_t num)
{
	switch(controlState)
	{
		case NONE:
			enableTune();
			display_tune();
			controlState = NONE;
			break;
		case A_MODE:
			KB_cancleCheckTimeOut();
			Ui_State = FIRING;
			chuTrinh = chuTrinhTam;
			phanDoan = 0;
			display_setAlarm(availableSegment[chuTrinh].timeSet[phanDoan]);
			RunningMode = A_MODE;
			controlState = NONE;
			break;
		case B_MODE:
			KB_cancleCheckTimeOut();
			Ui_State = FIRING;
			chuTrinh = chuTrinhTam;
			phanDoan = 0;
			display_setAlarm(unavailableSegment[chuTrinh].timeSet[phanDoan]);
			RunningMode = B_MODE;
			controlState = NONE;
			break;
		case F_MODE:
			PID_param.kp = (float)pidParam[KP]/100;
			PID_param.ki = (float)pidParam[TI]/100;
			PID_param.kd = (float)pidParam[TD]/100;
			PID_setParam_pid(&PID_param);
			Ui_returnLastState();
			controlState = NONE;
			break;
	}
}

void ESC_cb(uint8_t num)
{
	printf("esc callback\r\n");
	if(checkTuneDone() == 0)
	{
		lat = 1;
		cancleTune();
	}
	if(Ui_State == FIRING)
	{
		printf("break firing\r\n");
		Ui_breakFiring();
	}
	else if(controlState != NONE)
	{
		KB_cancleCheckTimeOut();
		Ui_returnLastState();
		controlState = NONE;
	}
	else lat = 1;
}

void CLEAR_cb(uint8_t num)
{
	switch(controlState)
	{
		case C_MODE:
			KB_enableCheckTimeOut();
			if(pos < 4)
			{
				temTemp[phanDoanTam] = 0;
				display_leftParam(temTemp[phanDoanTam]);
			}
			else
			{
				temTime[phanDoanTam] = 0;
				display_rightParam(temTime[phanDoanTam]);
			}
			break;
	}
}

void PAUSE_cb(uint8_t num)
{

}

void NEGATIVE_cb(uint8_t num)
{
	switch(controlState)
	{
		case C_MODE:
		{
			if(pos < 4 && temTemp[phanDoanTam] < 1000)
			{
				temTemp[phanDoanTam] = (-1)*temTemp[phanDoanTam];
				display_leftParam(temTemp[phanDoanTam]);
			}
			KB_enableCheckTimeOut();
			break;
		}
	}
}

void PGUP_cb(uint8_t num)
{
	switch(controlState)
	{
		case C_MODE:
			KB_enableCheckTimeOut();
			if(++phanDoanTam > 19) phanDoanTam = 0;
			display_leftParam(temTemp[phanDoanTam]);
			display_rightParam(temTime[phanDoanTam]);
			Ui_checkBlinkSingleLed(phanDoanTam);
			break;
		case E_MODE:
			KB_enableCheckTimeOut();
			if(++phanDoanTam == unavailableSegment[chuTrinhTam].maxSeg) phanDoanTam = 0;
			display_leftParam(unavailableSegment[chuTrinhTam].temSet[phanDoanTam]);
			display_rightParam(unavailableSegment[chuTrinhTam].timeSet[phanDoanTam]);
			Ui_checkBlinkSingleLed(phanDoanTam);
			break;
		case F_MODE:
			KB_enableCheckTimeOut();
			if(++chiSoPid > TD) chiSoPid = KP;
			display_setPID(chiSoPid, pidParam[chiSoPid]);
			break;
	}
}

void PGDN_cb(uint8_t num)
{
	switch(controlState)
	{
		case C_MODE:
			KB_enableCheckTimeOut();
			if((int8_t)--phanDoanTam < 0) phanDoanTam = 19;
			display_leftParam(temTemp[phanDoanTam]);
			display_rightParam(temTime[phanDoanTam]);
			Ui_checkBlinkSingleLed(phanDoanTam);
			break;
		case E_MODE:
			KB_enableCheckTimeOut();
			if((int8_t)--phanDoanTam < 0) phanDoanTam = unavailableSegment[chuTrinhTam].maxSeg - 1;
			display_leftParam(unavailableSegment[chuTrinhTam].temSet[phanDoanTam]);
			display_rightParam(unavailableSegment[chuTrinhTam].timeSet[phanDoanTam]);
			Ui_checkBlinkSingleLed(phanDoanTam);
			break;
		case F_MODE:
			KB_enableCheckTimeOut();
			if((int8_t)--chiSoPid < KP) chiSoPid = TD;
			display_setPID(chiSoPid, pidParam[chiSoPid]);
			break;
	}
}

void KB_btnCheckTimeOut(void)
{
	if(hasPress && checkPassTime(g_SysTime, timeWaitReset))
	{
		KB_btnTimeOut();
	}
}

void KB_btnTimeOut(void)
{
//	PRINTF("timeOut, dp = %d, ldp = %d\r\n", display_mode, last_display_mode);
	KB_cancleCheckTimeOut();
	Ui_returnLastState();
	controlState = 0;
}

void KB_enableCheckTimeOut(void)
{
	hasPress = true;
	timeWaitReset = g_SysTime + TIME_WAIT_MAX;
}

void KB_cancleCheckTimeOut(void)
{
	hasPress = false;
	timeWaitReset = g_SysTime;
}
