/*
 * keyboard.h
 *
 *  Created on: Feb 13, 2019
 *      Author: chungnguyen
 */

#ifndef KEYBOARD_KEYBOARD_H_
#define KEYBOARD_KEYBOARD_H_
#include "MKE04Z1284.h"
typedef enum
{
	ROW1_PIN = 0,
	ROW2_PIN,
	ROW3_PIN,
	ROW4_PIN,
	ROW5_PIN,
	MAX_ROW
} pinRow_t;

typedef enum
{
	COL1_PIN = 0,
	COL2_PIN,
	COL3_PIN,
	COL4_PIN,
	COL5_PIN,
	COL6_PIN,
	COL7_PIN,
	COL8_PIN,
	MAX_COL
} pinCol_t;

void KB_scan(void);
void KB_btnCheckTimeOut(void);
void KB_btnTimeOut(void);
void KB_enableCheckTimeOut(void);
void KB_cancleCheckTimeOut(void);
#endif /* KEYBOARD_KEYBOARD_H_ */
