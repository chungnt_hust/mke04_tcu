/*
 * Ui_control.c
 *
 *  Created on: Feb 14, 2019
 *      Author: chungnguyen
 */
#include "Ui_control.h"
#include "display.h"
#include "W25Q16JV.h"
#include "MAX31855.h"
#include "algorithm/controller/controller.h"
/*******************************************************************************
 * Definitions
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/
uint8_t RunningMode;
uint8_t Ui_State = INACTION;
uint8_t Ui_LastState;
uint8_t chuTrinh;
uint8_t phanDoan;
int16_t intTem, therTemp;
uint32_t time_now;
uint8_t lat = 1;
extern params_t availableSegment[10];
extern params_t unavailableSegment[10];
extern volatile uint32_t g_SysTime;
/*******************************************************************************
 * Code
 ******************************************************************************/
void Ui_process(void)
{
	if(checkTuneDone() == 0)
	{
		tuneProcess();
		//lat = 1;
	}
	else display_tuneDone();
	switch(Ui_State)
	{
		case INACTION:
			if(lat)
			{
				lat = 0;
				Ui_LastState = INACTION;
				for(uint8_t i = 0; i < MAX_LED7; i++)
				{
					Led7_showCharAt(negative, i);
					Led7_blink(i);
				}
				for(uint8_t i = 0; i < MAX_SINGLE_LED; i++)
				{
					SingleLed_canCleBlink(i, STATE_OFF);
				}
			}
			break;
		case SETTING:
			if(Ui_LastState == FIRING)
			{
				display_getRtc(&time_now);
				controller_tick();
			}
			lat = 1;
			break;
		case FIRING:
			MAX31855_readTemperature(&intTem, &therTemp);
			display_leftParam(therTemp); // thay 0 bằng nhiệt độ đo
			if(lat)
			{
				lat = 0;
				Ui_LastState = FIRING;
				if(RunningMode == 2) display_rightParam(unavailableSegment[chuTrinh].temSet[phanDoan]);
				if(RunningMode == 1) display_rightParam(availableSegment[chuTrinh].temSet[phanDoan]);
				for(uint8_t i = 0; i < MAX_LED7; i++) Led7_canCleBlink(i);
				Ui_checkBlinkSingleLed(phanDoan);
			}

			display_getRtc(&time_now);
			controller_tick();
			break;
		case PAUSE:
			break;
	}
}

void Ui_returnLastState(void)
{
	if(Ui_LastState == INACTION) Ui_State = INACTION;
	if(Ui_LastState == FIRING) Ui_State = FIRING;
}

void Ui_breakFiring(void)
{
	lat = 1;
	Ui_LastState = INACTION;
	Ui_State = INACTION;
}

void Ui_readFlashBegin(void)
{
	for(uint8_t readFlashCount = 0; readFlashCount < 10; readFlashCount++)
	{
//		PRINTF("%d\r\n", readFlashCount);
		W25Q16JV_Read(ERASE_SECTOR_ADD(readFlashCount), (uint8_t*)&unavailableSegment[readFlashCount], sizeof(unavailableSegment[readFlashCount]));
		for(uint32_t x = 0; x < 80000; x++) asm("NOP");

	}

	for(uint8_t readFlashCount = 0; readFlashCount < 10; readFlashCount++)
	{
//		PRINTF("chu trinh %d\r\n", readFlashCount);
		for(uint8_t pd = 0; pd < 20; pd++)
		{
			if(unavailableSegment[readFlashCount].temSet[pd] == (int16_t)0xFFFF) unavailableSegment[readFlashCount].temSet[pd] = 0;
			if(unavailableSegment[readFlashCount].timeSet[pd] == (uint16_t)0xFFFF) unavailableSegment[readFlashCount].timeSet[pd] = 0;
//    		PRINTF("temp[%d] = %d, ", pd, SegmentParams[readFlashCount].temSet[pd]);
//    		PRINTF("time[%d] = %d\r\n", pd, SegmentParams[readFlashCount].timeSet[pd]);
		}
	}
}

void Ui_callback(void)
{
	if(Ui_LastState == FIRING)
	{
		Ui_returnLastState();
		if(RunningMode == 2)
		{
			if(++phanDoan < unavailableSegment[chuTrinh].maxSeg)
			{
				display_setAlarm(unavailableSegment[chuTrinh].timeSet[phanDoan]);
				lat = 1;
			}
			else
			{
				display_show_end();
				Alarm_blink(5);
				Ui_LastState = Ui_State = INACTION;
			}
		}

		if(RunningMode == 1)
		{
			if(++phanDoan < availableSegment[chuTrinh].maxSeg)
			{
				display_setAlarm(availableSegment[chuTrinh].timeSet[phanDoan]);
				lat = 1;
			}
			else
			{
				display_show_end();
				Alarm_blink(5);
				Ui_LastState = Ui_State = INACTION;
			}
		}
	}
}

void Ui_checkBlinkSingleLed(uint8_t value)
{
	for(uint8_t i = 0; i < MAX_SINGLE_LED; i++)
	{
		SingleLed_canCleBlink(i, STATE_OFF);
	}
	if(value >= 9) // nếu đang ở phân đoạn lớn hơn bằng 9 thì bật hết led đến và nháy ở vị trí phân đoạn lấy dư cho 10
	{
		for(uint8_t i = 9; i < value; i++)
		{
			SingleLed_canCleBlink(i%MAX_SINGLE_LED, STATE_ON);
		}
	}
	SingleLed_blink(value%MAX_SINGLE_LED);
}


