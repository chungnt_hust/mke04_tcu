/*
 * Ui_control.h
 *
 *  Created on: Feb 14, 2019
 *      Author: chungnguyen
 */

#ifndef UI_CONTROL_UI_CONTROL_H_
#define UI_CONTROL_UI_CONTROL_H_
#include "MKE04Z1284.h"
enum
{
	INACTION = 0,
	SETTING,
	FIRING,
	PAUSE
};
typedef struct
{
	int16_t  temSet[20];
	uint16_t timeSet[20];
	uint8_t maxSeg;
} params_t;
void Ui_process(void);
void Ui_returnLastState(void);
void Ui_breakFiring(void);
void Ui_readFlashBegin(void);
void Ui_callback(void); // hàm xử lý khi kết thúc phân đoạn
void Ui_checkBlinkSingleLed(uint8_t value);
#endif /* UI_CONTROL_UI_CONTROL_H_ */
