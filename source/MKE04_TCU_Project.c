/*
 * Copyright 2016-2018 NXP
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    MKE04_TCU_Project.c
 * @brief   Application entry point.
 */
#include <debugUart/debugUart.h>
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MKE04Z1284.h"
/*******************************************************************************
 * Another include
 ******************************************************************************/
#include "Ssr.h"
#include "SpiUser.h"
#include "W25Q16JV.h"
#include "display.h"
#include "keyboard.h"
#include "Ui_control.h"
#include "algorithm/pid/pid.h"
/*******************************************************************************
 * Definitions
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/
volatile uint32_t g_SysTime;
volatile bool s_sysTickInt;
uint16_t s_TimeSlice[10];
/*******************************************************************************
 * Code
 ******************************************************************************/
void SysTick_Handler(void)
{
	Led7_IRQHandler();
	g_SysTime++;
	s_sysTickInt = true;
}
/*
 * @brief   Application entry point.
 */
int main(void) {
  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
    SysTick_Config(SystemCoreClock / 1000U);
    debugUart_Init();
//    Alarm_blink(3);
    SSR_PWM_Init();
    SPI_User_Init();
    display_InitRtc();

    Ui_readFlashBegin();
    PID_getParam();
    /* Enter an infinite loop, just incrementing a counter. */
    while(1)
    {
    	if(s_sysTickInt)
    	{
    		s_sysTickInt = false;
    		for(uint8_t i = 0; i < 10; i++) s_TimeSlice[i]++;
    		KB_scan();
    	}

    	if(s_TimeSlice[0] >= 10)
    	{
    		s_TimeSlice[0] = 0;
    		display_process();
    	}

    	if(s_TimeSlice[1] >= 100)
    	{
    		s_TimeSlice[1] = 0;
    		W25Q16JV_WriteProcess();
    		Ui_process();
    	}

    	if(s_TimeSlice[2] >= 1000)
    	{
    		s_TimeSlice[2] = 0;
    		KB_btnCheckTimeOut();
    	}

    }
    return 0 ;
}
