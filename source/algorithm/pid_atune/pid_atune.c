/*
 * pid_atune.c
 *
 *  Created on: Jan 28, 2019
 *      Author: ngoct
 */

#define millis() g_SysTime
#include "pid_atune.h"
//#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
extern volatile uint32_t g_SysTime;

uint8_t isMax, isMin;
//double *input, *output;
double setpoint;
double noiseBand;
int controlType;
bool running;
unsigned long peak1, peak2, lastTime;
int sampleTime;
int nLookBack;
int peakType;
double lastInputs[101];
double peaks[10];
int peakCount;
uint8_t justchanged;
uint8_t justevaled;
double absMax, absMin;
double oStep;
double outputStart;
double Ku, Pu;

atune_param_t atune_param ;

void PID_ATune_Init(int16_t* Input, uint8_t* Output)
{
	atune_param.input = Input;
	atune_param.output = Output;
	controlType = 1 ; //default to PID
	noiseBand = 5;
	outputStart = 50;
	running = 0;
	oStep = 10;
//	*Output = outputStart;
	PID_ATune_SetLookbackSec(20);
	lastTime = millis();
	printf("\r\nATune Init Done");
}
void PID_ATune_Cancel()
{
	running = 0;
}

int PID_ATune_Runtime()
{
	justevaled = 0;
	if(peakCount>9 && running)
	{
		running = 0;
		PID_ATune_FinishUp();
		printf("atune finish\r\n");
		return 1;
	}
	unsigned long now = g_SysTime;

	if((now-lastTime)<sampleTime)
	{
		printf("---\r\n");
		return 0;
	}
	lastTime = now;
	double refVal = *(atune_param.input);
	justevaled=1;
	if(!running)
	{ //initialize working variables the first time around
		peakType = 0;
		peakCount=0;
		justchanged=0;
		absMax=refVal;
		absMin=refVal;
		setpoint = refVal;
		running = 1;
		outputStart = *(atune_param.output);
		*(atune_param.output) = outputStart+oStep;
		printf("no running\r\n");
	}
	else
	{
		if(refVal>absMax)absMax=refVal;
		if(refVal<absMin)absMin=refVal;
		printf("running\r\n");
	}

	//oscillate the output base on the input's relation to the setpoint

	if(refVal>setpoint+noiseBand) *(atune_param.output) = outputStart-oStep;
	else if (refVal<setpoint-noiseBand) *(atune_param.output) = outputStart+oStep;


  //bool isMax=1, isMin=1;
  isMax=1;isMin=1;
  //id peaks
  for(int i=nLookBack-1;i>=0;i--)
  {
    double val = lastInputs[i];
    if(isMax) isMax = refVal>val;
    if(isMin) isMin = refVal<val;
    lastInputs[i+1] = lastInputs[i];
  }
  lastInputs[0] = refVal;
  if(nLookBack<9)
  {  //we don't want to trust the maxes or mins until the inputs array has been filled
	return 0;
	}

  if(isMax)
  {
    if(peakType==0)peakType=1;
    if(peakType==-1)
    {
      peakType = 1;
      justchanged=1;
      peak2 = peak1;
    }
    peak1 = now;
    peaks[peakCount] = refVal;

  }
  else if(isMin)
  {
    if(peakType==0)peakType=-1;
    if(peakType==1)
    {
      peakType=-1;
      peakCount++;
      justchanged=1;
    }

    if(peakCount<10) peaks[peakCount] = refVal;
  }

  if(justchanged && peakCount>2)
  { //we've transitioned.  check if we can autotune based on the last peaks
    double avgSeparation = (abs(peaks[peakCount-1]-peaks[peakCount-2])+abs(peaks[peakCount-2]-peaks[peakCount-3]))/2;
    if( avgSeparation < 0.05*(absMax-absMin))
    {
    	PID_ATune_FinishUp();
      running = 0;
	  return 1;

    }
  }
   justchanged=0;
	return 0;
}
void PID_ATune_FinishUp()
{
	*(atune_param.output) = outputStart;
      //we can generate tuning parameters!
      Ku = 4*(2*oStep)/((absMax-absMin)*3.14159);
      Pu = (double)(peak1-peak2) / 1000;
}

double PID_ATune_GetKp()
{
	return controlType==1 ? 0.6 * Ku : 0.4 * Ku;
}

double PID_ATune_GetKi()
{
	return controlType==1? 1.2*Ku / Pu : 0.48 * Ku / Pu;  // Ki = Kc/Ti
}

double PID_ATune_GetKd()
{
	return controlType==1? 0.075 * Ku * Pu : 0;  //Kd = Kc * Td
}

void PID_ATune_SetOutputStep(double Step)
{
	oStep = Step;
}

double PID_ATune_GetOutputStep()
{
	return oStep;
}

void PID_ATune_SetControlType(int Type) //0=PI, 1=PID
{
	controlType = Type;
}
int PID_ATune_GetControlType()
{
	return controlType;
}

void PID_ATune_SetNoiseBand(double Band)
{
	noiseBand = Band;
}

double PID_ATune_GetNoiseBand()
{
	return noiseBand;
}

void PID_ATune_SetLookbackSec(int value)
{
    if (value<1) value = 1;

	if(value<25)
	{
		nLookBack = value * 4;
		sampleTime = 250;
	}
	else
	{
		nLookBack = 100;
		sampleTime = value*10;
	}
}

int PID_ATune_GetLookbackSec()
{
	return nLookBack * sampleTime / 1000;
}
