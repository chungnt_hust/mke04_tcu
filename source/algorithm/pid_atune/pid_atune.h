/*
 * pid_atune.h
 *
 *  Created on: Dec 27, 2018
 *      Author: ngoct
 */

#ifndef PID_ATUNE_PID_ATUNE_H_
#define PID_ATUNE_PID_ATUNE_H_

#include "MKE04Z1284.h"

typedef struct {
	int16_t* input;
	uint8_t* output;
}atune_param_t;



//PID_ATune(double*, double*);                       	// * Constructor.  links the Autotune to a given PID
    void PID_ATune_Init(int16_t* Input, uint8_t* Output);
	int PID_ATune_Runtime();						   			   	// * Similar to the PID Compue function, returns non 0 when done
	void PID_ATune_Cancel();									   	// * Stops the AutoTune

	void PID_ATune_SetOutputStep(double);						   	// * how far above and below the starting value will the output step?
	double PID_ATune_GetOutputStep();							   	//

	void PID_ATune_SetControlType(int); 						   	// * Determies if the tuning parameters returned will be PI (D=0)
	int PID_ATune_GetControlType();							   	//   or PID.  (0=PI, 1=PID)

	void PID_ATune_SetLookbackSec(int);							// * how far back are we looking to identify peaks
	int PID_ATune_GetLookbackSec();								//

	void PID_ATune_SetNoiseBand(double);							// * the autotune will ignore signal chatter smaller than this value
	double PID_ATune_GetNoiseBand();								//   this should be acurately set

	double PID_ATune_GetKp();										// * once autotune is complete, these functions contain the
	double PID_ATune_GetKi();										//   computed tuning parameters.
	double PID_ATune_GetKd();
	void PID_ATune_FinishUp();

#endif /* PID_ATUNE_PID_ATUNE_H_ */
