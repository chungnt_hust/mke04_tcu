/*
 * path.h
 *
 *  Created on: Jan 2, 2019
 *      Author: ngoct
 */

#ifndef PATH_PATH_H_
#define PATH_PATH_H_

/*****************/
#define temp_offset_index 0
#define time_offset_index 20
#define nop_index         40

#include "MKE04Z1284.h"
#include "display.h"

typedef struct
{
	uint8_t num_of_point;   //so dinh tren duong doan nhiet
	uint8_t time_step_s;    //buoc thoi gian gan setpoint
	uint16_t cycle[2][21];  //duong doan nhiet thoi gian - nhiet do
	uint16_t timecycle[21]; //do thi thoi gian
}path_t;
void getParam(uint8_t index, uint16_t *ptr);
float get_setpoint(path_t* path, uint8_t passed_point_index, uint16_t time_now);
void get_numofpoint(path_t* path);
void set_timestep(path_t* path, int timestep);
void build_path(path_t* path);



#endif /* PATH_PATH_H_ */
