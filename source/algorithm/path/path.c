/*
 * path.c
 *
 *  Created on: Jan 2, 2019
 *      Author: ngoct
 */

#include "path.h"
#include "W25Q16JV.h"
#include "Ui_control.h"
/* global variable */
path_t path;
extern params_t param_now ;

void getParam(uint8_t index, uint16_t *ptr)
{
	if (index < 20)
		*ptr = param_now.temSet[index];
	else if (index < 40)
		*ptr = param_now.timeSet[index - 20];
	else if (index == 40)
		*ptr = param_now.maxSeg;
}
float get_setpoint(path_t* path, uint8_t passed_point_index, uint16_t time_now)
{
	uint16_t time_1 = path->cycle[1][passed_point_index];
	uint16_t time_2 = path->cycle[1][passed_point_index + 1];
	uint16_t temp_1 = path->cycle[0][passed_point_index];
	uint16_t temp_2 = path->cycle[0][passed_point_index + 1];
	uint8_t timestep = path->time_step_s;
	return (float)temp_1 + (time_now * 60 + timestep - time_1) * (temp_2 - temp_1) / (time_2 - time_1);
}
void get_numofpoint(path_t* path)
{
	uint16_t nop ;
	getParam(nop_index,&nop);
	path->num_of_point = nop;
}
void set_timestep(path_t* path, int timestep)
{
	path->time_step_s = timestep;
}
void build_path(path_t* path)
{
	uint8_t i = 0;
	uint16_t sum_of_time = 0;
	for(i = 0; i < path->num_of_point; i++)
		{
			uint16_t temp;
			uint16_t time;
			getParam(temp_offset_index + i, &temp);
			getParam(time_offset_index + i, &time);
			path->cycle[0][i] = temp;
			path->cycle[1][i] = time;
			sum_of_time += time;
			path->timecycle[i] = sum_of_time;
		}
}

