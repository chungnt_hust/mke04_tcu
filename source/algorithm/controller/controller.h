/*
 * controller.h
 *
 *  Created on: Jan 28, 2019
 *      Author: ngoct
 */

#ifndef CONTROLLER_CONTROLLER_H_
#define CONTROLLER_CONTROLLER_H_

#define WAIT_TIME 1*10*10

#include "algorithm/path/path.h"
#include "algorithm/pid/pid.h"
#include "algorithm/pid_atune/pid_atune.h"
#include "MAX31855.h"
#include "MKE04Z1284.h"
#include "W25Q16JV.h"

void set_tuned(uint8_t val);
uint8_t get_tuned(void);
void controller_run();
void controller_tune();
void controller_init();
void controller_tick();

void enableTune(void);
void cancleTune(void);
void tuneProcess(void);
uint8_t checkTuneDone(void);
//void aTune_init();




#endif /* CONTROLLER_CONTROLLER_H_ */
