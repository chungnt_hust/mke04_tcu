/*
 * controller.c
 *
 *  Created on: Jan 29, 2019
 *      Author: ngoct
 */


/*
 * controller.cpp
 *
 *  Created on: Dec 27, 2018
 *      Author: ngoct
 */

#include "controller.h"
#include "stdio.h"
#include "Ssr.h"
extern uint32_t time_now;
extern int16_t therTemp; ;
extern uint8_t chuTrinh;
extern uint8_t phanDoan;
uint8_t controller_output;
pid_t temp_controller ;
extern pid_param_t PID_param ;
extern atune_param_t atune_param ;
extern path_t path;
int path_index = 1;
uint8_t controller_init_done = 0;
uint8_t need_to_tune = 0;
uint8_t tuneDone = 0;
uint16_t wait_stable_tick = 0;

void set_tuned(uint8_t val)
{
	uint8_t byte = val;
	W25Q16JV_Write(ERASE_SECTOR_ADD(200), &byte, sizeof(byte));
}
uint8_t get_tuned(void)
{
	uint8_t byte;
	W25Q16JV_Read(ERASE_SECTOR_ADD(200), &byte, sizeof(byte));
	return byte;
}
void controller_run()
{
	float setpoint = get_setpoint(&path, phanDoan, time_now);
	temp_controller.cur_point = therTemp;
	temp_controller.set_point = setpoint;
	controller_output = (int)PID_Calc(&temp_controller);
	SSR_PWM_SetDuty(controller_output);
	printf("\r\n.");
}

void controller_tune()
{
	uint8_t val = (PID_ATune_Runtime());
	SSR_PWM_SetDuty(controller_output);
	//printf("exited PID_ATune_Runtime\r\n");
	if(val != 0)
	{ //we're done, set the tuning parameters
		PID_param.kp = (float)PID_ATune_GetKp();
		PID_param.ki = (float)PID_ATune_GetKi();
		PID_param.kd = (float)PID_ATune_GetKd();
		PID_setParam_pid(&PID_param);
		//set_tuned(1);
		need_to_tune = 0;
		tuneDone = 1;
		printf("\r\nATune Done");
	}
}
void controller_init()
{
	printf("\r\nController Init ...");
//	uint8_t tune = get_tuned();
//
//	if(tune == 0 || tune == 0xFF)
//	{
		/* khoi tao bo auto tune pid*/
		PID_ATune_Init(&therTemp, &controller_output);
		need_to_tune = 1;
//	}
//	else
//	{
//		/* khoi tao bo dieu khien pid*/
//		getParam_pid(&PID_param);
//		temp_controller.kp = PID_param.kp;
//		temp_controller.ki = PID_param.ki;
//		temp_controller.kd = PID_param.kd;
//		PID_Init(&temp_controller);
//		controller_init_done = 1;
//		printf("\r\nController Init Done");
//	}
}
void controller_tick()
{
//	if (need_to_tune)
//	{
//		wait_stable_tick++;
//		if(wait_stable_tick > WAIT_TIME)
//		{
//			printf("\r\nStart Tuning..");
//			wait_stable_tick = WAIT_TIME;
//			controller_tune();
//		}
//		else
//		{
//			if (wait_stable_tick % (10*10) == 0)
//				printf("\r\n%d",wait_stable_tick/600);
//			controller_output = 20;
//		}
//	}
//	else
//	{
		if (controller_init_done)
			controller_run();
		else
		{
			temp_controller.kp = PID_param.kp;
			temp_controller.ki = PID_param.ki;
			temp_controller.kd = PID_param.kd;
			PID_Init(&temp_controller);
			controller_init_done = 1;
			printf("\r\nController Init Done");
		}
//	}
}

/*void aTune_init()
{
	uint8_t aTuneStep = 10, aTuneNoise = 5, aTuneStartValue = 50;
	uint8_t aTuneLookBack = 20;
    //Set the output to the desired starting frequency.
    controller_output=aTuneStartValue;
    PID_ATune_SetNoiseBand(aTuneNoise);
    PID_ATune_SetOutputStep(aTuneStep);
    PID_ATune_SetLookbackSec((int)aTuneLookBack);
}*/

void enableTune(void)
{
	PID_ATune_Init(&therTemp, &controller_output);
	need_to_tune = 1;
	tuneDone = 0;
}

void cancleTune(void)
{
	need_to_tune = 0;
}

void tuneProcess(void)
{
	if (need_to_tune)
	{
		wait_stable_tick++;
		if(wait_stable_tick > WAIT_TIME)
		{
			printf("\r\nStart Tuning..");
			wait_stable_tick = WAIT_TIME;
			controller_tune();
		}
		else
		{
			if (wait_stable_tick % (10*10) == 0)
				printf("\r\n%d",wait_stable_tick/600);
			controller_output = 20;
		}
	}
}

uint8_t checkTuneDone(void)
{
	return tuneDone;
}
