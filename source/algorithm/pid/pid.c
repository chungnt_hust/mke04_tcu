/*
 * pid.c
 *
 *  Created on: Dec 27, 2018
 *      Author: ngoct
 */

#include "pid.h"
#include "W25Q16JV.h"

#define OUTMAX 100
#define OUTMIN 0

pid_param_t PID_param ;
extern int16_t pidParam[3];
extern uint8_t chiSoPid;

void PID_getParam(void)
{
	getParam_pid(&PID_param);
}
void getParam_pid(pid_param_t* pid_param)
{
	uint32_t len = sizeof(*pid_param);
	W25Q16JV_Read(PID_PARAMS_ADD , (uint8_t*)pid_param , len);
	pidParam[0] = PID_param.kp*100;
	pidParam[1] = PID_param.ki*100;
	pidParam[2] = PID_param.kd*100;
}
void PID_setParam_pid(pid_param_t* pid_param)
{
	uint32_t len = sizeof(*pid_param);
	W25Q16JV_Write(PID_PARAMS_ADD , (uint8_t*)pid_param , len);
}
void PID_Init(pid_t* PID)
{
	PID->integral = 0.0;
	PID->sample_time = 100;
	PID->cur_point = 0.0;
	PID->error_prev = 0.0;
}
float PID_Calc(pid_t* PID)
{
	PID->error = PID->set_point - PID->cur_point;
	PID->integral = PID->integral + (PID->error * PID->sample_time / 1000);
	// chong bao hoa tich phan.
	if (PID->integral > OUTMAX)
			PID->integral = OUTMAX;
	else if (PID->integral < OUTMIN)
			PID->integral = OUTMIN;

	PID->output = PID->kp * PID->error + PID->ki * PID->integral + PID->kd * (PID->error - PID->error_prev)/(PID->sample_time / 1000);
	PID->error_prev = PID->error;
	if (PID->output > OUTMAX)
		PID->output = OUTMAX;
	else if (PID->output < OUTMIN)
		PID->output = OUTMIN;
	return PID->output;
}
