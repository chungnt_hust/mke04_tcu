/*
 * pid.h
 *
 *  Created on: Dec 27, 2018
 *      Author: ngoct
 */

#ifndef PID_PID_H_
#define PID_PID_H_

typedef struct {
	float kp;
	float kd;
	float ki;

	float set_point;
	float cur_point;

	float sample_time;
	float integral;
	float error;
	float error_prev;
	float output;
}pid_t;

typedef struct {
	float kp;
	float kd;
	float ki;
}pid_param_t;
void PID_getParam(void);
void PID_setParam_pid(pid_param_t* pid_param);
void PID_Init(pid_t* PID);
float PID_Calc(pid_t* PID);
void getParam_pid(pid_param_t* pid_param);
void setParam_pid(pid_param_t* pid_param);

#endif /* PID_PID_H_ */
