/*
 * Ssr.c
 *
 *  Created on: Feb 13, 2019
 *      Author: chungnguyen
 */
#include <Ssr.h>
#include "fsl_ftm.h"
/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define SSR_PWM_IRQ_HANDLE   FTM2_IRQHandler
#define SSR_PWM_ADDRESS      FTM2
#define SSR_PWM_CHANNEL_FLAG kFTM_Chnl2Flag
#define SSR_PWM_CHANNEL      kFTM_Chnl_2
#define SSR_PWM_CHNL_IT_EN   kFTM_Chnl2InterruptEnable
#define SSR_PWM_IRQ_NUMBER   FTM2_IRQn
/*******************************************************************************
 * Variables
 ******************************************************************************/
ftm_config_t ftmInfo;
ftm_chnl_pwm_signal_param_t ftmParam;
ftm_pwm_level_select_t pwmLevel = kFTM_HighTrue;
/*******************************************************************************
 * Code
 ******************************************************************************/
void SSR_PWM_IRQ_HANDLE(void)
{
    if ((FTM_GetStatusFlags(SSR_PWM_ADDRESS) & SSR_PWM_CHANNEL_FLAG) == SSR_PWM_CHANNEL_FLAG)
    {
    	/* Clear interrupt flag.*/
    	FTM_ClearStatusFlags(SSR_PWM_ADDRESS, SSR_PWM_CHANNEL_FLAG);
    }
}

void SSR_PWM_Init(void)
{
	/* Configure ftm params with frequency 20kHZ */
	ftmParam.chnlNumber = SSR_PWM_CHANNEL;
	ftmParam.level = pwmLevel;
	ftmParam.dutyCyclePercent = 50;
	ftmParam.firstEdgeDelayPercent = 0U;

	FTM_GetDefaultConfig(&ftmInfo);
	/* Initialize FTM module */
	FTM_Init(SSR_PWM_ADDRESS, &ftmInfo);

	/* Enable channel interrupt flag.*/
	FTM_EnableInterrupts(SSR_PWM_ADDRESS, SSR_PWM_CHNL_IT_EN);

	/* Enable at the NVIC */
	EnableIRQ(SSR_PWM_IRQ_NUMBER);
	//printf("Init PWM done!!\r\n");
	SSR_PWM_SetHz(20000);
}

void SSR_PWM_SetHz(uint32_t Hz)
{
	FTM_SetupPwm(SSR_PWM_ADDRESS, &ftmParam, 1U, kFTM_CenterAlignedPwm, Hz, CLOCK_GetFreq(kCLOCK_TimerClk));
	FTM_StartTimer(SSR_PWM_ADDRESS, kFTM_SystemClock);
}

void SSR_PWM_SetDuty(uint8_t duty)
{
	if(duty >= 100) duty = 99;
	if(duty <= 0) duty = 1;

	FTM_StopTimer(SSR_PWM_ADDRESS);
	 /* Disable interrupt to retain current dutycycle for a few seconds */
	FTM_DisableInterrupts(SSR_PWM_ADDRESS, SSR_PWM_CHNL_IT_EN);
	/* Disable channel output before updating the dutycycle */
	FTM_UpdateChnlEdgeLevelSelect(SSR_PWM_ADDRESS, SSR_PWM_CHANNEL, 0U);
	/* Update PWM duty cycle */
	FTM_UpdatePwmDutycycle(SSR_PWM_ADDRESS, SSR_PWM_CHANNEL, kFTM_CenterAlignedPwm, duty);
	/* Software trigger to update registers */
	FTM_SetSoftwareTrigger(SSR_PWM_ADDRESS, true);
	/* Start channel output with updated dutycycle */
	FTM_UpdateChnlEdgeLevelSelect(SSR_PWM_ADDRESS, SSR_PWM_CHANNEL, pwmLevel);
	/* Enable interrupt flag to update PWM dutycycle */
	FTM_EnableInterrupts(SSR_PWM_ADDRESS, SSR_PWM_CHNL_IT_EN);

	FTM_StartTimer(SSR_PWM_ADDRESS, kFTM_SystemClock);
}
