/*
 * Ssr.h
 *
 *  Created on: Feb 13, 2019
 *      Author: chungnguyen
 */

#ifndef SSR_SSR_H_
#define SSR_SSR_H_
#include "MKE04Z1284.h"
void SSR_PWM_Init(void);

/* @biref: set frequency
 * @param: Hz
 * @reval: NONE
 * */
void SSR_PWM_SetHz(uint32_t Hz);

/* @biref: set duty
 * @param: duty: update duty circle
 * @reval: NONE
 * */
void SSR_PWM_SetDuty(uint8_t duty);

#endif /* SSR_SSR_H_ */
