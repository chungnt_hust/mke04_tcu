/*
 * SingleLed.c
 *
 *  Created on: Feb 13, 2019
 *      Author: chungnguyen
 */
#include <display/SingleLed/SingleLed.h>
#include "timeCheck.h"
#include <stdbool.h>
/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define TIME_ON  200
#define TIME_OFF 200
/*******************************************************************************
 * Variables
 ******************************************************************************/
static GPIO_Type *gpioPort[] = {GPIOA, GPIOA, GPIOA, GPIOA, GPIOA, GPIOB, GPIOB, GPIOC, GPIOA, GPIOB};
static uint32_t valuePin[]   = {1<<16, 1<<17, 1<<29, 1<<31, 1<<19, 1<<5,  1<<24, 1<<4,  1<<15, 1<<7};
struct
{
	uint32_t timeOn;
	uint32_t timeOff;
	bool isOn;
	bool isOff;
	bool isBlink;
	uint8_t state;
} SingleLed_Type[10];

extern volatile uint32_t g_SysTime;
/*******************************************************************************
 * Code
 ******************************************************************************/
void SingleLed_On(pinSingleLed_t ledNum)
{
	gpioPort[ledNum]->PCOR = valuePin[ledNum];
}

void SingleLed_Off(pinSingleLed_t ledNum)
{
	gpioPort[ledNum]->PSOR = valuePin[ledNum];
}

void SingleLed_onInMs(pinSingleLed_t ledNum, uint32_t msOn)
{
	SingleLed_Type[ledNum].timeOn = g_SysTime + msOn;
	SingleLed_Type[ledNum].isOn = true;
	SingleLed_On(ledNum);
}

void SingleLed_offInMs(pinSingleLed_t ledNum, uint32_t msOff)
{
	SingleLed_Type[ledNum].timeOff = g_SysTime + msOff;
	SingleLed_Type[ledNum].isOff = true;
	SingleLed_Off(ledNum);
}

void SingleLed_blink(pinSingleLed_t ledNum)
{
	SingleLed_Type[ledNum].isBlink = true;
	SingleLed_onInMs(ledNum, TIME_ON);
}

void SingleLed_canCleBlink(pinSingleLed_t ledNum, StateLed_t stateAfter)
{
	SingleLed_Type[ledNum].isBlink = false;
	if(stateAfter == STATE_OFF) SingleLed_Off(ledNum);
	if(stateAfter == STATE_ON)  SingleLed_On(ledNum);
}

void SingleLed_process(void)
{
	static uint8_t countCheckBlink = 0;
	for(uint8_t ledNum = 0; ledNum < MAX_SINGLE_LED; ledNum++)
	{
		if(SingleLed_Type[ledNum].isBlink)
		{
			if(SingleLed_Type[ledNum].isOn && checkPassTime(g_SysTime, SingleLed_Type[ledNum].timeOn))
			{
				//SingleLed_offInMs(ledNum, TIME_OFF);
				SingleLed_Type[ledNum].state = STATE_OFF;
				SingleLed_Type[ledNum].isOn = false;
			}
			else
			if(SingleLed_Type[ledNum].isOff && checkPassTime(g_SysTime, SingleLed_Type[ledNum].timeOff))
			{
				SingleLed_Type[ledNum].state = STATE_ON;
				SingleLed_Type[ledNum].isOff = false;
			}
		}
	}

	if(++countCheckBlink >= 10)
	{
		countCheckBlink = 0;
		for(uint8_t ledNum = 0; ledNum < MAX_SINGLE_LED; ledNum++)
		{
			if(SingleLed_Type[ledNum].state == STATE_OFF)
			{
				SingleLed_offInMs(ledNum, TIME_OFF);
				SingleLed_Type[ledNum].state = STATE_NONE;
			}
			if(SingleLed_Type[ledNum].state == STATE_ON)
			{
				SingleLed_onInMs(ledNum, TIME_ON);
				SingleLed_Type[ledNum].state = STATE_NONE;
			}
		}
	}
}

