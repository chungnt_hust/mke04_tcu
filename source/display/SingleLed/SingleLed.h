/*
 * SingleLed.h
 *
 *  Created on: Feb 13, 2019
 *      Author: chungnguyen
 */

#ifndef DISPLAY_SINGLELED_SINGLELED_H_
#define DISPLAY_SINGLELED_SINGLELED_H_
#include "MKE04Z1284.h"

typedef enum
{
	STATE_NONE = 0,
	STATE_ON,
	STATE_OFF,
} StateLed_t;

typedef enum
{
	BAR1_PIN = 0,
	BAR2_PIN,
	BAR3_PIN,
	BAR4_PIN,
	BAR5_PIN,
	BAR6_PIN,
	BAR7_PIN,
	BAR8_PIN,
	BAR9_PIN,
	BAR10_PIN,
	MAX_SINGLE_LED
} pinSingleLed_t;
void SingleLed_On(pinSingleLed_t ledNum);
void SingleLed_Off(pinSingleLed_t ledNum);
void SingleLed_blink(pinSingleLed_t ledNum);
void SingleLed_canCleBlink(pinSingleLed_t ledNum, StateLed_t stateAfter);
void SingleLed_process(void);
#endif /* DISPLAY_SINGLELED_SINGLELED_H_ */
