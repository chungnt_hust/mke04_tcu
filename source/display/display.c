/*
 * display.c
 *
 *  Created on: Feb 13, 2019
 *      Author: chungnguyen
 */
#include "display.h"
#include "Ui_control.h"
#include <stdlib.h>
/*******************************************************************************
 * Definitions
 ******************************************************************************/

/*******************************************************************************
 * Variables
 ******************************************************************************/
rtc_datetime_t date = {0};
/*******************************************************************************
 * Code
 ******************************************************************************/
void RTC_AlarmCallback(void)
{
	Ui_callback();
}

void display_InitRtc(void)
{
    rtc_config_t rtcConfig;
    /* Init RTC */
    /*
     * config->clockSource = kRTC_BusClock;
     * config->prescaler = kRTC_ClockDivide_16_2048;
     * config->time_us = 1000000U;
     */
    RTC_GetDefaultConfig(&rtcConfig);
    RTC_Init(RTC, &rtcConfig);


    //RTC_SetAlarm(25200);
    RTC_SetAlarmCallback(RTC_AlarmCallback);
    /* Enable RTC Interrupt */
    RTC_EnableInterrupts(RTC, kRTC_InterruptEnable);
    NVIC_EnableIRQ(RTC_IRQn);
}

void display_setAlarm(uint32_t second)
{
	date.hour = date.minute = date.second = 0;
	RTC_SetDatetime(&date);
	RTC_SetAlarm(second);
}

void display_getRtc(uint32_t *time)
{
	RTC_GetDatetime(&date);
	*time = date.hour*3600 + date.minute*60 + date.second;
}

void display_process(void)
{
	Alarm_process();
	SingleLed_process();
	Led7_process();
}

void display_show_program(modeProgram_t mode)
{
	for(uint8_t i = 0; i < MAX_LED7; i++)
	{
		Led7_showCharAt(OFF_LED, i);
		Led7_canCleBlink(i);
	}

	for(uint8_t ledNum = 0; ledNum < MAX_SINGLE_LED; ledNum++)
	{
		SingleLed_canCleBlink(ledNum, STATE_OFF);
	}

	if(mode != PRO_C_MODE)
	{
		Led7_showCharAt(P_code, 4);
		Led7_showCharAt(r_code, 5);
		if(mode == PRO_A_MODE) Led7_showCharAt(A_code, 6);
		if(mode == PRO_B_MODE) Led7_showCharAt(b_code, 6);
		if(mode == PRO_E_MODE) { Led7_showCharAt(E_code, 6); SingleLed_blink(BAR1_PIN); }
		Led7_showCharAt(__code, 7);
		Led7_blink(6);
		Led7_blink(7);
	}
	else if(mode == PRO_C_MODE)
	{
		display_leftParam(0);
		display_rightParam(0);
		SingleLed_blink(BAR1_PIN);
		Led7_blink(7);
	}
}

void display_setPID(uint8_t param, int16_t value)
{
	for(uint8_t i = 0; i < MAX_LED7; i++) Led7_canCleBlink(i);
	if(param == KP) Led7_showCharAt(P_code, 0);
	if(param == TI) Led7_showCharAt(i_code, 0);
	if(param == TD) Led7_showCharAt(d_code, 0);
	Led7_showCharAt(__code, 1);
	Led7_showCharAt(__code, 2);
	Led7_showCharAt(__code, 3);
	display_rightParam(value);
	Led7_blink(7);
}

void display_show_end(void)
{
	for(uint8_t i = 0; i < MAX_LED7; i++)
	{
		Led7_showCharAt(OFF_LED, i);
		Led7_canCleBlink(i);
	}

	for(uint8_t ledNum = 0; ledNum < MAX_SINGLE_LED; ledNum++)
	{
		SingleLed_canCleBlink(ledNum, STATE_OFF);
	}

	Led7_showCharAt(E_code, 4);
	Led7_showCharAt(n_code, 5);
	Led7_showCharAt(d_code, 6);
	Led7_blink(4);
	Led7_blink(5);
	Led7_blink(6);
}

void display_leftParam(int16_t value)
{
	int16_t x = value;
	int8_t index;
	for(index = 0; index < 4; index++) Led7_showCharAt(OFF_LED, index);
	index = 3;
	do
	{
		Led7_showCharAt(abs(value%10), index);
		value /= 10;
		index--;
	}while(value);
	if(x < 0) Led7_showCharAt(negative, index);
}

void display_rightParam(int16_t value)
{
	int16_t x = value;
	int8_t index;
	for(index = 4; index < 8; index++) Led7_showCharAt(OFF_LED, index);
	index = 7;
	do
	{
		Led7_showCharAt(abs(value%10), index);
		value /= 10;
		index--;
	}while(value);
	if(x < 0) Led7_showCharAt(negative, index);
}

void display_tune(void)
{
	for(uint8_t i = 0; i < MAX_LED7; i++)
	{
		Led7_showCharAt(OFF_LED, i);
		Led7_canCleBlink(i);
	}

	for(uint8_t ledNum = 0; ledNum < MAX_SINGLE_LED; ledNum++)
	{
		SingleLed_canCleBlink(ledNum, STATE_OFF);
	}

	Led7_showCharAt(t_code, 0);
	Led7_showCharAt(u_code, 1);
	Led7_showCharAt(n_code, 2);
	Led7_showCharAt(E_code, 3);
	Led7_showCharAt(__code, 4);
	Led7_showCharAt(__code, 5);
	Led7_showCharAt(__code, 6);
	Led7_showCharAt(__code, 7);
	Led7_blink(4);
	Led7_blink(5);
	Led7_blink(6);
	Led7_blink(7);
}

void display_tuneDone(void)
{
	Led7_showCharAt(d_code, 4);
	Led7_showCharAt(o_code, 5);
	Led7_showCharAt(n_code, 6);
	Led7_showCharAt(E_code, 7);
	for(uint8_t i = 0; i < MAX_LED7; i++)
	{
		Led7_canCleBlink(i);
	}
}
