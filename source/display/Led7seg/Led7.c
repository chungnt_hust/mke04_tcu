/*
 * led7.c
 *
 *  Created on: Feb 13, 2019
 *      Author: chungnguyen
 */
#include <display/Led7seg/Led7.h>
#include "timeCheck.h"
#include <stdbool.h>
/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define TIME_ON  300
#define TIME_OFF 300
/*******************************************************************************
 * Variables
 ******************************************************************************/
static GPIO_Type *gpioPortSegment[] = {GPIOB, GPIOA, GPIOA, GPIOB, GPIOA, GPIOB, GPIOA, GPIOB};
static GPIO_Type *gpioPortDigit[]   = {GPIOB, GPIOA, GPIOB, GPIOA, GPIOB, GPIOA, GPIOA, GPIOB};
static uint32_t valuePinSegment[]   = {1<<30, 1<<30, 1<<13, 1<<25, 1<<14, 1<<26, 1<<18, 1<<6};
static uint32_t valuePinDigit[]     = {1<<13, 1<<9,  1<<12, 1<<11, 1<<14, 1<<10, 1<<8,  1<<15};
struct
{
	uint32_t timeOn;
	uint32_t timeOff;
	bool isOn;
	bool isOff;
	bool isBlink;
	uint8_t state;
	uint8_t temCode;
} Led7_Type[8];

const uint8_t Led7_Code[] = {0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F,
							 0x00, 0x73, 0x50, 0x08, 0x77, 0x7C, 0x79, 0x54, 0x5E, 0x40, 0x10, 0x46, 0x1C, 0x5C};
uint8_t Led7_Buffer[8];
extern volatile uint32_t g_SysTime;
/*******************************************************************************
 * Code
 ******************************************************************************/
static inline void Led7_onSegment(segment_pin_t segment)
{
	gpioPortSegment[segment]->PSOR = valuePinSegment[segment];
}

static inline void Led7_offSegment(segment_pin_t segment)
{
	gpioPortSegment[segment]->PCOR = valuePinSegment[segment];
}

static inline void Led7_onDigit(digit_pin_t digit)
{
	gpioPortDigit[digit]->PSOR = valuePinDigit[digit];
}

static inline void Led7_offDigit(digit_pin_t digit)
{
	gpioPortDigit[digit]->PCOR = valuePinDigit[digit];
}

static inline void Led7_decode(uint8_t code)
{
	for(uint8_t segment = 0; segment < 8; segment++)
	{
		if(code & (0x01<<segment)) Led7_onSegment(segment);
		else Led7_offSegment(segment);
	}
}

static inline void Led7_show(digit_pin_t digit)
{
	for(uint8_t i = 0; i < 8; i++)
	{
		if(i == digit) Led7_onDigit(i);
		else Led7_offDigit(i);
	}
}

void Led7_IRQHandler(void)
{
	static uint8_t ledIndex = 0;
	Led7_decode(Led7_Code[OFF_LED]);
	Led7_show(ledIndex);
	Led7_decode(Led7_Buffer[ledIndex]);
	if(++ledIndex == 8) ledIndex = 0;
}

static void Led7_onInMs(digit_pin_t digit, uint32_t msOn)
{
	Led7_Type[digit].timeOn = g_SysTime + msOn;
	Led7_Type[digit].isOn = true;
	Led7_Buffer[digit] = Led7_Type[digit].temCode;
	Led7_Type[digit].isOff = false;
	Led7_Type[digit].timeOff = g_SysTime;
}

static void Led7_offInMs(digit_pin_t digit, uint32_t msOff)
{
	Led7_Type[digit].timeOff = g_SysTime + msOff;
	Led7_Type[digit].isOff = true;
	Led7_Type[digit].temCode = Led7_Buffer[digit];
	Led7_Buffer[digit] = Led7_Code[OFF_LED];
	Led7_Type[digit].isOn = false;
	Led7_Type[digit].timeOn = g_SysTime;
}

void Led7_blink(digit_pin_t digit)
{
	Led7_Type[digit].isBlink = true;
	Led7_onInMs(digit, TIME_ON);
}

void Led7_canCleBlink(digit_pin_t digit)
{
	Led7_Buffer[digit] = Led7_Type[digit].temCode;
	Led7_Type[digit].isBlink = false;
}

void Led7_process(void)
{
	static uint8_t countCheckBlink = 0;
	for(uint8_t ledNum = 0; ledNum < MAX_LED7; ledNum++)
	{
		if(Led7_Type[ledNum].isBlink)
		{
			if(Led7_Type[ledNum].isOn && checkPassTime(g_SysTime, Led7_Type[ledNum].timeOn))
			{
				Led7_Type[ledNum].state = STATE_LE7_OFF;
				Led7_Type[ledNum].isOn = false;
			}
			else
			if(Led7_Type[ledNum].isOff && checkPassTime(g_SysTime, Led7_Type[ledNum].timeOff))
			{
				Led7_Type[ledNum].state = STATE_LE7_ON;
				Led7_Type[ledNum].isOff = false;
			}
		}
	}

	if(++countCheckBlink >= 5)
	{
		countCheckBlink = 0;
		for(uint8_t ledNum = 0; ledNum < MAX_LED7; ledNum++)
		{
			if(Led7_Type[ledNum].isBlink)
			{
				if(Led7_Type[ledNum].state == STATE_LE7_OFF)
				{
					Led7_offInMs(ledNum, TIME_OFF);
					Led7_Type[ledNum].state = STATE_LE7_NONE;
				}
				if(Led7_Type[ledNum].state == STATE_LE7_ON)
				{
					Led7_onInMs(ledNum, TIME_ON);
					Led7_Type[ledNum].state = STATE_LE7_NONE;
				}
			}
		}
	}
}

void Led7_showCharAt(uint8_t character, digit_pin_t digit)
{
	Led7_Type[digit].temCode = Led7_Buffer[digit] = Led7_Code[character];
}
