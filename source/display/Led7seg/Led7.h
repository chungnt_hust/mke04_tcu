/*
 * Led7.h
 *
 *  Created on: Feb 13, 2019
 *      Author: chungnguyen
 */

#ifndef DISPLAY_LED7SEG_LED7_H_
#define DISPLAY_LED7SEG_LED7_H_
#include "MKE04Z1284.h"

enum
{
	OFF_LED  = 10, //0x00
	P_code	 = 11, //0x73 // P
	r_code   = 12, //0x50 // r
	__code   = 13, //0x08 // _
	A_code   = 14, //0x77 // A
	b_code   = 15, //0x7C // b
	E_code   = 16, //0x79 // E
	n_code   = 17, //0x54 // n
	d_code   = 18, //0x5E // d
	negative = 19, //0x40 // -
	i_code   = 20,
	t_code   = 21,
	u_code   = 22,
	o_code   = 23
};

typedef enum
{
	STATE_LE7_NONE = 0,
	STATE_LE7_ON,
	STATE_LE7_OFF,
} StateLed7_t;

typedef enum
{
	LEDA_PIN = 0,
	LEDB_PIN,
	LEDC_PIN,
	LEDD_PIN,
	LEDE_PIN,
	LEDF_PIN,
	LEDG_PIN,
	LEDH_PIN
} segment_pin_t;

typedef enum
{
	LED1_PIN = 0,
	LED2_PIN,
	LED3_PIN,
	LED4_PIN,
	LED5_PIN,
	LED6_PIN,
	LED7_PIN,
	LED8_PIN,
	MAX_LED7
} digit_pin_t;

void Led7_IRQHandler(void);
void Led7_blink(digit_pin_t digit);
void Led7_canCleBlink(digit_pin_t digit);
void Led7_process(void);
void Led7_showCharAt(uint8_t character, digit_pin_t digit);
#endif /* DISPLAY_LED7SEG_LED7_H_ */
