/*
 * display.h
 *
 *  Created on: Feb 13, 2019
 *      Author: chungnguyen
 */

#ifndef DISPLAY_DISPLAY_H_
#define DISPLAY_DISPLAY_H_
#include <display/Alarm/Alarm.h>
#include <display/Led7seg/Led7.h>
#include <display/SingleLed/SingleLed.h>
#include "fsl_rtc.h"
typedef enum
{
	PRO_A_MODE = 1,
	PRO_B_MODE,
	PRO_C_MODE,
	PRO_E_MODE,
} modeProgram_t;
enum
{
	KP = 0,
	TI,
	TD
};
void display_InitRtc(void);
void display_setAlarm(uint32_t second);
void display_getRtc(uint32_t *time);
void display_process(void);
void display_show_program(modeProgram_t mode);
void display_show_end(void);
void display_leftParam(int16_t value);
void display_rightParam(int16_t value);
void display_setPID(uint8_t param, int16_t value);
void display_tune(void);
void display_tuneDone(void);
#endif /* DISPLAY_DISPLAY_H_ */
