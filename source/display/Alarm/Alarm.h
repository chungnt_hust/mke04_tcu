/*
 * Alarm.h
 *
 *  Created on: Feb 13, 2019
 *      Author: chungnguyen
 */

#ifndef DISPLAY_ALARM_ALARM_H_
#define DISPLAY_ALARM_ALARM_H_
#include "MKE04Z1284.h"
void Alarm_blink(uint8_t time);
void Alarm_canCleBlink(void);
void Alarm_process(void);

#endif /* DISPLAY_ALARM_ALARM_H_ */
