/*
 * Alarm.c
 *
 *  Created on: Feb 13, 2019
 *      Author: chungnguyen
 */
#include <display/Alarm/Alarm.h>
#include "timeCheck.h"
#include <stdbool.h>
/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define TIME_ON  50
#define TIME_OFF 200
/*******************************************************************************
 * Variables
 ******************************************************************************/
enum
{
	ALARM_PIN = 0,
} pinAlarm_t;
static GPIO_Type *gpioPort[] = {GPIOA};
static uint32_t valuePin[] = {1<<25}; // PTD1
struct
{
	uint32_t timeOn;
	uint32_t timeOff;
	bool isOn;
	bool isOff;
	uint8_t blinkTime;
} Alarm_Type;

extern volatile uint32_t g_SysTime;
/*******************************************************************************
 * Code
 ******************************************************************************/
void Alarm_On(void)
{
	gpioPort[ALARM_PIN]->PSOR = valuePin[ALARM_PIN];
}

void Alarm_Off(void)
{
	gpioPort[ALARM_PIN]->PCOR = valuePin[ALARM_PIN];
}

void Alarm_onInMs(uint32_t msOn)
{
	Alarm_Type.timeOn = g_SysTime + msOn;
	Alarm_Type.isOn = true;
	Alarm_On();
}

void Alarm_offInMs(uint32_t msOff)
{
	Alarm_Type.timeOff = g_SysTime + msOff;
	Alarm_Type.isOff = true;
	Alarm_Off();
}

void Alarm_blink(uint8_t time)
{
	Alarm_Type.blinkTime = time;
	if(time) Alarm_onInMs(TIME_ON);
	else Alarm_Off();
}

void Alarm_canCleBlink(void)
{
	Alarm_blink(0);
}

void Alarm_process(void)
{
	if(Alarm_Type.blinkTime)
	{
		if(Alarm_Type.isOn && checkPassTime(g_SysTime, Alarm_Type.timeOn))
		{
			Alarm_offInMs(TIME_OFF);
			Alarm_Type.isOn = false;
		}
		else
		if(Alarm_Type.isOff && checkPassTime(g_SysTime, Alarm_Type.timeOff))
		{
			Alarm_Type.blinkTime--;
			if(Alarm_Type.blinkTime) Alarm_onInMs(TIME_ON);
			Alarm_Type.isOff = false;
		}
	}
}
