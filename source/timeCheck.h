/*
 * timeCheck.h
 *
 *  Created on: Dec 15, 2018
 *      Author: chungnguyen
 */

#ifndef TIMECHECK_H_
#define TIMECHECK_H_

#include "MKE04Z1284.h"
#include <stdbool.h>

uint32_t checkTimeOut(uint32_t newTime, uint32_t oldTime);
bool checkPassTime(uint32_t newTime, uint32_t timeCheck);

#endif /* TIMECHECK_H_ */
