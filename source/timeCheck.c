/*
 * timeCheck.c
 *
 *  Created on: Dec 15, 2018
 *      Author: chungnguyen
 */
#include "timeCheck.h"

inline uint32_t checkTimeOut(uint32_t newTime, uint32_t oldTime)
{
	return (newTime - oldTime);
}

inline bool checkPassTime(uint32_t newTime, uint32_t timeCheck)
{
	return (checkTimeOut(newTime, timeCheck) < 1000000)?true:false;
}

